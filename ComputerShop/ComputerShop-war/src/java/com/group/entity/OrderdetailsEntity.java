/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "orderdetails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderdetailsEntity.findAll", query = "SELECT o FROM OrderdetailsEntity o"),
    @NamedQuery(name = "OrderdetailsEntity.findByOrderDetailId", query = "SELECT o FROM OrderdetailsEntity o WHERE o.orderDetailId = :orderDetailId"),
    @NamedQuery(name = "OrderdetailsEntity.buyBest", query = "SELECT o.productId,COUNT(o.productId) FROM OrderdetailsEntity o GROUP BY o.productId HAVING COUNT(o.productId)>=:top ORDER BY COUNT(o.productId)DESC"),
    @NamedQuery(name = "OrderdetailsEntity.findByOrderQuantity", query = "SELECT o FROM OrderdetailsEntity o WHERE o.orderQuantity = :orderQuantity")})
public class OrderdetailsEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OrderDetailId")
    private Integer orderDetailId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OrderQuantity")
    private int orderQuantity;
    @JoinColumn(name = "OrderId", referencedColumnName = "OrderId")
    @ManyToOne(optional = false)
    private OrdersEntity orderId;
    @JoinColumn(name = "ProductId", referencedColumnName = "ProductId")
    @ManyToOne(optional = false)
    private ProductsEntity productId;
    private int top;

    public OrderdetailsEntity() {
    }

    public OrderdetailsEntity(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public OrderdetailsEntity(Integer orderDetailId, int orderQuantity) {
        this.orderDetailId = orderDetailId;
        this.orderQuantity = orderQuantity;
    }

    public Integer getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public OrdersEntity getOrderId() {
        return orderId;
    }

    public void setOrderId(OrdersEntity orderId) {
        this.orderId = orderId;
    }

    public ProductsEntity getProductId() {
        return productId;
    }

    public void setProductId(ProductsEntity productId) {
        this.productId = productId;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderDetailId != null ? orderDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderdetailsEntity)) {
            return false;
        }
        OrderdetailsEntity other = (OrderdetailsEntity) object;
        if ((this.orderDetailId == null && other.orderDetailId != null) || (this.orderDetailId != null && !this.orderDetailId.equals(other.orderDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.OrderdetailsEntity[ orderDetailId=" + orderDetailId + " ]";
    }
    
}
