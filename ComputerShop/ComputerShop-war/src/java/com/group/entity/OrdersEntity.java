/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "orders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrdersEntity.findAll", query = "SELECT o FROM OrdersEntity o"),
    @NamedQuery(name = "OrdersEntity.findByOrderId", query = "SELECT o FROM OrdersEntity o WHERE o.orderId = :orderId"),
    @NamedQuery(name = "OrdersEntity.findByAmount", query = "SELECT o FROM OrdersEntity o WHERE o.amount = :amount"),
    @NamedQuery(name = "OrdersEntity.findByOrderDate", query = "SELECT o FROM OrdersEntity o WHERE o.orderDate = :orderDate"),
    @NamedQuery(name = "OrdersEntity.findByOrderShippingStatus", query = "SELECT o FROM OrdersEntity o WHERE o.orderShippingStatus = :orderShippingStatus"),
    @NamedQuery(name = "OrdersEntity.findByPaymentType", query = "SELECT o FROM OrdersEntity o WHERE o.paymentType = :paymentType"),
    @NamedQuery(name = "OrdersEntity.findByCurrencyCode", query = "SELECT o FROM OrdersEntity o WHERE o.currencyCode = :currencyCode"),
    @NamedQuery(name = "OrdersEntity.findByShippingStreet", query = "SELECT o FROM OrdersEntity o WHERE o.shippingStreet = :shippingStreet"),
    @NamedQuery(name = "OrdersEntity.findByShippingCountry", query = "SELECT o FROM OrdersEntity o WHERE o.shippingCountry = :shippingCountry"),
    @NamedQuery(name = "OrdersEntity.getMax", query = "SELECT MAX(o.orderId) FROM OrdersEntity o"),
    @NamedQuery(name = "OrdersEntity.findByPhoneNumber", query = "SELECT o FROM OrdersEntity o WHERE o.phoneNumber = :phoneNumber")})
public class OrdersEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OrderId")
    private Integer orderId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OrderDate")
    @Temporal(TemporalType.DATE)
    private Date orderDate;
    @Column(name = "OrderShippingStatus")
    private Integer orderShippingStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "PaymentType")
    private String paymentType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CurrencyCode")
    private String currencyCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ShippingStreet")
    private String shippingStreet;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ShippingCountry")
    private String shippingCountry;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @JoinColumn(name = "CustomerId", referencedColumnName = "CustomerId")
    @ManyToOne(optional = false)
    private CustomersEntity customerId;
    @JoinColumn(name = "DistrictId", referencedColumnName = "DistrictId")
    @ManyToOne(optional = false)
    private DistrictsEntity districtId;
    @JoinColumn(name = "ProvinceId", referencedColumnName = "ProvinceId")
    @ManyToOne(optional = false)
    private ProvincesEntity provinceId;

    public OrdersEntity() {
    }

    public OrdersEntity(Integer orderId) {
        this.orderId = orderId;
    }

    public OrdersEntity(Integer orderId, double amount, Date orderDate, String paymentType, String currencyCode, String shippingStreet, String shippingCountry, String phoneNumber) {
        this.orderId = orderId;
        this.amount = amount;
        this.orderDate = orderDate;
        this.paymentType = paymentType;
        this.currencyCode = currencyCode;
        this.shippingStreet = shippingStreet;
        this.shippingCountry = shippingCountry;
        this.phoneNumber = phoneNumber;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getOrderShippingStatus() {
        return orderShippingStatus;
    }

    public void setOrderShippingStatus(Integer orderShippingStatus) {
        this.orderShippingStatus = orderShippingStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getShippingStreet() {
        return shippingStreet;
    }

    public void setShippingStreet(String shippingStreet) {
        this.shippingStreet = shippingStreet;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public CustomersEntity getCustomerId() {
        return customerId;
    }

    public void setCustomerId(CustomersEntity customerId) {
        this.customerId = customerId;
    }

    public DistrictsEntity getDistrictId() {
        return districtId;
    }

    public void setDistrictId(DistrictsEntity districtId) {
        this.districtId = districtId;
    }

    public ProvincesEntity getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(ProvincesEntity provinceId) {
        this.provinceId = provinceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderId != null ? orderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdersEntity)) {
            return false;
        }
        OrdersEntity other = (OrdersEntity) object;
        if ((this.orderId == null && other.orderId != null) || (this.orderId != null && !this.orderId.equals(other.orderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.OrdersEntity[ orderId=" + orderId + " ]";
    }
    
}
