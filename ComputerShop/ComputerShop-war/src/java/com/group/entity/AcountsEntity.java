/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "acounts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcountsEntity.findAll", query = "SELECT a FROM AcountsEntity a"),
    @NamedQuery(name = "AcountsEntity.findByAcountId", query = "SELECT a FROM AcountsEntity a WHERE a.acountId = :acountId"),
    @NamedQuery(name = "AcountsEntity.findByUserName", query = "SELECT a FROM AcountsEntity a WHERE a.userName = :userName"),
    @NamedQuery(name = "AcountsEntity.findByPassword", query = "SELECT a FROM AcountsEntity a WHERE a.password = :password"),
    @NamedQuery(name = "findByNamAndPassword", query = "SELECT a FROM AcountsEntity a WHERE a.userName = :userName AND a.password = :password AND a.status = 1"),
    @NamedQuery(name = "AcountsEntity.findByStatus", query = "SELECT a FROM AcountsEntity a WHERE a.status = :status")})
public class AcountsEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "AcountId")
    private Integer acountId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "UserName")
    private String userName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "Password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Status")
    private int status;
    @JoinColumn(name = "EmployeeId", referencedColumnName = "EmployeeId")
    @ManyToOne(optional = false)
    private EmployeesEntity employeeId;
    @JoinColumn(name = "RoleId", referencedColumnName = "RoleId")
    @ManyToOne(optional = false)
    private RolesEntity roleId;

    public AcountsEntity() {
    }

    public AcountsEntity(Integer acountId) {
        this.acountId = acountId;
    }

    public AcountsEntity(Integer acountId, String userName, String password, int status) {
        this.acountId = acountId;
        this.userName = userName;
        this.password = password;
        this.status = status;
    }

    public Integer getAcountId() {
        return acountId;
    }

    public void setAcountId(Integer acountId) {
        this.acountId = acountId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public EmployeesEntity getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(EmployeesEntity employeeId) {
        this.employeeId = employeeId;
    }

    public RolesEntity getRoleId() {
        return roleId;
    }

    public void setRoleId(RolesEntity roleId) {
        this.roleId = roleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (acountId != null ? acountId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcountsEntity)) {
            return false;
        }
        AcountsEntity other = (AcountsEntity) object;
        if ((this.acountId == null && other.acountId != null) || (this.acountId != null && !this.acountId.equals(other.acountId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.AcountsEntity[ acountId=" + acountId + " ]";
    }

}
