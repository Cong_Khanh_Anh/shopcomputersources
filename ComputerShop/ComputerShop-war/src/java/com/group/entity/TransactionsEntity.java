/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "transactions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransactionsEntity.findAll", query = "SELECT t FROM TransactionsEntity t"),
    @NamedQuery(name = "TransactionsEntity.findByTransactionId", query = "SELECT t FROM TransactionsEntity t WHERE t.transactionId = :transactionId"),
    @NamedQuery(name = "TransactionsEntity.getMax", query = "SELECT MAX(t.transactionId) FROM TransactionsEntity t"),
    @NamedQuery(name = "TransactionsEntity.findByResponseCode", query = "SELECT t FROM TransactionsEntity t WHERE t.responseCode = :responseCode"),
    @NamedQuery(name = "TransactionsEntity.findByStatus", query = "SELECT t FROM TransactionsEntity t WHERE t.status = :status"),
    @NamedQuery(name = "TransactionsEntity.findByNumber", query = "SELECT t FROM TransactionsEntity t WHERE t.number = :number"),
    @NamedQuery(name = "TransactionsEntity.findByTransactionDate", query = "SELECT t FROM TransactionsEntity t WHERE t.transactionDate = :transactionDate"),
    @NamedQuery(name = "TransactionsEntity.findByMessage", query = "SELECT t FROM TransactionsEntity t WHERE t.message = :message")})
public class TransactionsEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "TransactionId")
    private String transactionId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ResponseCode")
    private String responseCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "Number")
    private String number;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TransactionDate")
    @Temporal(TemporalType.DATE)
    private Date transactionDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Message")
    private String message;
    @JoinColumn(name = "OrderId", referencedColumnName = "OrderId")
    @ManyToOne(optional = false)
    private OrdersEntity orderId;

    public TransactionsEntity() {
    }

    public TransactionsEntity(String transactionId) {
        this.transactionId = transactionId;
    }

    public TransactionsEntity(String transactionId, String responseCode, String status, String number, Date transactionDate, String message) {
        this.transactionId = transactionId;
        this.responseCode = responseCode;
        this.status = status;
        this.number = number;
        this.transactionDate = transactionDate;
        this.message = message;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrdersEntity getOrderId() {
        return orderId;
    }

    public void setOrderId(OrdersEntity orderId) {
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionId != null ? transactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionsEntity)) {
            return false;
        }
        TransactionsEntity other = (TransactionsEntity) object;
        if ((this.transactionId == null && other.transactionId != null) || (this.transactionId != null && !this.transactionId.equals(other.transactionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.TransactionsEntity[ transactionId=" + transactionId + " ]";
    }
    
}
