/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductsEntity.findAll", query = "SELECT p FROM ProductsEntity p"),
    @NamedQuery(name = "ProductsEntity.findByProductId", query = "SELECT p FROM ProductsEntity p WHERE p.productId = :productId"),
    @NamedQuery(name = "ProductsEntity.findByName", query = "SELECT p FROM ProductsEntity p WHERE p.name LIKE :name"),
    @NamedQuery(name = "ProductsEntity.findByDescription", query = "SELECT p FROM ProductsEntity p WHERE p.description = :description"),
    @NamedQuery(name = "ProductsEntity.findByRepresentPicture", query = "SELECT p FROM ProductsEntity p WHERE p.representPicture = :representPicture"),
    @NamedQuery(name = "ProductsEntity.findBySaleUnit", query = "SELECT p FROM ProductsEntity p WHERE p.saleUnit = :saleUnit"),
    @NamedQuery(name = "ProductsEntity.findByPrice", query = "SELECT p FROM ProductsEntity p WHERE p.price = :price"),
    @NamedQuery(name = "ProductsEntity.findByQuantity", query = "SELECT p FROM ProductsEntity p WHERE p.quantity = :quantity"),
    @NamedQuery(name = "ProductsEntity.findByViewClick", query = "SELECT p FROM ProductsEntity p WHERE p.viewClick >= :viewClick ORDER BY P.viewClick DESC"),
    @NamedQuery(name = "ProductsEntity.findByDateOfProduction", query = "SELECT p FROM ProductsEntity p WHERE p.dateOfProduction = :dateOfProduction"),
    @NamedQuery(name = "ProductsEntity.findByWarrantyPeriod", query = "SELECT p FROM ProductsEntity p WHERE p.warrantyPeriod = :warrantyPeriod"),
    @NamedQuery(name = "ProductsEntity.findByScreenSize", query = "SELECT p FROM ProductsEntity p WHERE p.screenSize = :screenSize"),
    @NamedQuery(name = "ProductsEntity.findByWeight", query = "SELECT p FROM ProductsEntity p WHERE p.weight = :weight")})
public class ProductsEntity implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private Collection<OrderdetailsEntity> orderdetailsEntityCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ProductId")
    private Integer productId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Name")
    private String name;
    @Size(max = 150)
    @Column(name = "Description")
    private String description;
    @Size(max = 50)
    @Column(name = "RepresentPicture")
    private String representPicture;
    @Size(max = 20)
    @Column(name = "SaleUnit")
    private String saleUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Price")
    private double price;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Quantity")
    private int quantity;
    @Column(name = "ViewClick")
    private Integer viewClick;
    @Column(name = "DateOfProduction")
    @Temporal(TemporalType.DATE)
    private Date dateOfProduction;
    @Column(name = "WarrantyPeriod")
    private Integer warrantyPeriod;
    @Size(max = 20)
    @Column(name = "ScreenSize")
    private String screenSize;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Weight")
    private Double weight;
    @JoinColumn(name = "CategoryId", referencedColumnName = "CategoryId")
    @ManyToOne(optional = false)
    private CategorysEntity categoryId;
    @JoinColumn(name = "PictureId", referencedColumnName = "PictureId")
    @ManyToOne(optional = false)
    private PicturesEntity pictureId;

    public ProductsEntity() {
    }

    public ProductsEntity(Integer productId) {
        this.productId = productId;
    }

    public ProductsEntity(Integer productId, String name, double price, int quantity) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRepresentPicture() {
        return representPicture;
    }

    public void setRepresentPicture(String representPicture) {
        this.representPicture = representPicture;
    }

    public String getSaleUnit() {
        return saleUnit;
    }

    public void setSaleUnit(String saleUnit) {
        this.saleUnit = saleUnit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Integer getViewClick() {
        return viewClick;
    }

    public void setViewClick(Integer viewClick) {
        this.viewClick = viewClick;
    }

    public Date getDateOfProduction() {
        return dateOfProduction;
    }

    public void setDateOfProduction(Date dateOfProduction) {
        this.dateOfProduction = dateOfProduction;
    }

    public Integer getWarrantyPeriod() {
        return warrantyPeriod;
    }

    public void setWarrantyPeriod(Integer warrantyPeriod) {
        this.warrantyPeriod = warrantyPeriod;
    }

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        this.screenSize = screenSize;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public CategorysEntity getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(CategorysEntity categoryId) {
        this.categoryId = categoryId;
    }

    public PicturesEntity getPictureId() {
        return pictureId;
    }

    public void setPictureId(PicturesEntity pictureId) {
        this.pictureId = pictureId;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductsEntity)) {
            return false;
        }
        ProductsEntity other = (ProductsEntity) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.ProductsEntity[ productId=" + productId + " ]";
    }

    @XmlTransient
    public Collection<OrderdetailsEntity> getOrderdetailsEntityCollection() {
        return orderdetailsEntityCollection;
    }

    public void setOrderdetailsEntityCollection(Collection<OrderdetailsEntity> orderdetailsEntityCollection) {
        this.orderdetailsEntityCollection = orderdetailsEntityCollection;
    }
    
}
