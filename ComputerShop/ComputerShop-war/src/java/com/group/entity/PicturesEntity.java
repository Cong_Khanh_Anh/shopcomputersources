/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "pictures")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PicturesEntity.findAll", query = "SELECT p FROM PicturesEntity p"),
    @NamedQuery(name = "PicturesEntity.findByPictureId", query = "SELECT p FROM PicturesEntity p WHERE p.pictureId = :pictureId"),
    @NamedQuery(name = "PicturesEntity.findByDescription", query = "SELECT p FROM PicturesEntity p WHERE p.description LIKE :description"),
    @NamedQuery(name = "PicturesEntity.findByPicture1", query = "SELECT p FROM PicturesEntity p WHERE p.picture1 = :picture1"),
    @NamedQuery(name = "PicturesEntity.findByPicture2", query = "SELECT p FROM PicturesEntity p WHERE p.picture2 = :picture2"),
    @NamedQuery(name = "PicturesEntity.findByPicture3", query = "SELECT p FROM PicturesEntity p WHERE p.picture3 = :picture3"),
    @NamedQuery(name = "PicturesEntity.findByPicture4", query = "SELECT p FROM PicturesEntity p WHERE p.picture4 = :picture4")})
public class PicturesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PictureId")
    private Integer pictureId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Description")
    private String description;
    @Size(max = 50)
    @Column(name = "Picture1")
    private String picture1;
    @Size(max = 50)
    @Column(name = "Picture2")
    private String picture2;
    @Size(max = 50)
    @Column(name = "Picture3")
    private String picture3;
    @Size(max = 50)
    @Column(name = "Picture4")
    private String picture4;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pictureId")
    private Collection<ProductsEntity> productsEntityCollection;

    public PicturesEntity() {
    }

    public PicturesEntity(Integer pictureId) {
        this.pictureId = pictureId;
    }

    public PicturesEntity(Integer pictureId, String description) {
        this.pictureId = pictureId;
        this.description = description;
    }

    public Integer getPictureId() {
        return pictureId;
    }

    public void setPictureId(Integer pictureId) {
        this.pictureId = pictureId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture1() {
        return picture1;
    }

    public void setPicture1(String picture1) {
        this.picture1 = picture1;
    }

    public String getPicture2() {
        return picture2;
    }

    public void setPicture2(String picture2) {
        this.picture2 = picture2;
    }

    public String getPicture3() {
        return picture3;
    }

    public void setPicture3(String picture3) {
        this.picture3 = picture3;
    }

    public String getPicture4() {
        return picture4;
    }

    public void setPicture4(String picture4) {
        this.picture4 = picture4;
    }

    @XmlTransient
    public Collection<ProductsEntity> getProductsEntityCollection() {
        return productsEntityCollection;
    }

    public void setProductsEntityCollection(Collection<ProductsEntity> productsEntityCollection) {
        this.productsEntityCollection = productsEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pictureId != null ? pictureId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PicturesEntity)) {
            return false;
        }
        PicturesEntity other = (PicturesEntity) object;
        if ((this.pictureId == null && other.pictureId != null) || (this.pictureId != null && !this.pictureId.equals(other.pictureId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.PicturesEntity[ pictureId=" + pictureId + " ]";
    }
    
}
