/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "address")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AddressEntity.findAll", query = "SELECT a FROM AddressEntity a"),
    @NamedQuery(name = "AddressEntity.findByAddressId", query = "SELECT a FROM AddressEntity a WHERE a.addressId = :addressId"),
    @NamedQuery(name = "AddressEntity.findByStreetName", query = "SELECT a FROM AddressEntity a WHERE a.streetName = :streetName")})
public class AddressEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "AddressId")
    private Integer addressId;
    @Size(max = 100)
    @Column(name = "StreetName")
    private String streetName;
    @JoinColumn(name = "DistrictId", referencedColumnName = "DistrictId")
    @ManyToOne(optional = false)
    private DistrictsEntity districtId;
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "adressId")
    private Collection<CustomersEntity> customersEntityCollection;*/
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "addressId")
    private Collection<EmployeesEntity> employeesEntityCollection;*/

    public AddressEntity() {
    }

    public AddressEntity(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public DistrictsEntity getDistrictId() {
        return districtId;
    }

    public void setDistrictId(DistrictsEntity districtId) {
        this.districtId = districtId;
    }

    /*@XmlTransient
    public Collection<CustomersEntity> getCustomersEntityCollection() {
        return customersEntityCollection;
    }

    public void setCustomersEntityCollection(Collection<CustomersEntity> customersEntityCollection) {
        this.customersEntityCollection = customersEntityCollection;
    }*/

    /*@XmlTransient
    public Collection<EmployeesEntity> getEmployeesEntityCollection() {
        return employeesEntityCollection;
    }

    public void setEmployeesEntityCollection(Collection<EmployeesEntity> employeesEntityCollection) {
        this.employeesEntityCollection = employeesEntityCollection;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressId != null ? addressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AddressEntity)) {
            return false;
        }
        AddressEntity other = (AddressEntity) object;
        if ((this.addressId == null && other.addressId != null) || (this.addressId != null && !this.addressId.equals(other.addressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.AddressEntity[ addressId=" + addressId + " ]";
    }
    
}
