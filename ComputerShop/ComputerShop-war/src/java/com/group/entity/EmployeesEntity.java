/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vancong
 */
@Entity
@Table(name = "employees")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployeesEntity.findAll", query = "SELECT e FROM EmployeesEntity e"),
    @NamedQuery(name = "EmployeesEntity.findByEmployeeId", query = "SELECT e FROM EmployeesEntity e WHERE e.employeeId = :employeeId"),
    @NamedQuery(name = "EmployeesEntity.findByFirstName", query = "SELECT e FROM EmployeesEntity e WHERE e.firstName LIKE :firstName"),
    @NamedQuery(name = "EmployeesEntity.findByLastName", query = "SELECT e FROM EmployeesEntity e WHERE e.lastName = :lastName"),
    @NamedQuery(name = "EmployeesEntity.findByGender", query = "SELECT e FROM EmployeesEntity e WHERE e.gender = :gender"),
    @NamedQuery(name = "EmployeesEntity.findByDateOfBirth", query = "SELECT e FROM EmployeesEntity e WHERE e.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name = "EmployeesEntity.findByEmail", query = "SELECT e FROM EmployeesEntity e WHERE e.email = :email"),
    @NamedQuery(name = "EmployeesEntity.findByPhoneNumber", query = "SELECT e FROM EmployeesEntity e WHERE e.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "EmployeesEntity.findByAddress", query = "SELECT e FROM EmployeesEntity e WHERE e.address = :address")})
public class EmployeesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "EmployeeId")
    private Integer employeeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "FirstName")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "LastName")
    private String lastName;
    @Size(max = 10)
    @Column(name = "Gender")
    private String gender;
    @Column(name = "DateOfBirth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Email")
    private String email;
    @Size(max = 11)
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Address")
    private String address;
    @JoinColumn(name = "DistrictId", referencedColumnName = "DistrictId")
    @ManyToOne(optional = false)
    private DistrictsEntity districtId;
    @JoinColumn(name = "ProvinceId", referencedColumnName = "ProvinceId")
    @ManyToOne(optional = false)
    private ProvincesEntity provinceId;

    public EmployeesEntity() {
    }

    public EmployeesEntity(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public EmployeesEntity(Integer employeeId, String firstName, String lastName, String email, String address) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public DistrictsEntity getDistrictId() {
        return districtId;
    }

    public void setDistrictId(DistrictsEntity districtId) {
        this.districtId = districtId;
    }

    public ProvincesEntity getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(ProvincesEntity provinceId) {
        this.provinceId = provinceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeId != null ? employeeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeesEntity)) {
            return false;
        }
        EmployeesEntity other = (EmployeesEntity) object;
        if ((this.employeeId == null && other.employeeId != null) || (this.employeeId != null && !this.employeeId.equals(other.employeeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.group.entity.EmployeesEntity[ employeeId=" + employeeId + " ]";
    }
    
}
