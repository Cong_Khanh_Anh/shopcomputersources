/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.EmployeesEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author vancong
 */
@Stateless
public class EmployeesEntityFacade extends AbstractFacade<EmployeesEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeesEntityFacade() {
        super(EmployeesEntity.class);
    }
    
    public List<EmployeesEntity> findByName(String name){
        javax.persistence.Query q=em.createNamedQuery("EmployeesEntity.findByFirstName");
        q.setParameter("firstName", "%"+name+"%");
        return q.getResultList();
    }
}
