/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.CategorysEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
/**
 *
 * @author vancong
 */
@Stateless
public class CategorysEntityFacade extends AbstractFacade<CategorysEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategorysEntityFacade() {
        super(CategorysEntity.class);
    }

    public List<CategorysEntity> findByName(String name) {
         Query query = em.createNamedQuery("CategorysEntity.findByName");
         query.setParameter("name", "%" + name + "%");
         return query.getResultList();
    }
    
}
