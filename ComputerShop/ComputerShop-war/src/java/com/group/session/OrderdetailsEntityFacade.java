/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.OrderdetailsEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vancong
 */
@Stateless
public class OrderdetailsEntityFacade extends AbstractFacade<OrderdetailsEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrderdetailsEntityFacade() {
        super(OrderdetailsEntity.class);
    }
    
    public List<OrderdetailsEntity> buyBest(int top){
        Query q=em.createNamedQuery("OrderdetailsEntity.buyBest");
        q.setParameter("top", top);
        return q.getResultList();
    }
    
}
