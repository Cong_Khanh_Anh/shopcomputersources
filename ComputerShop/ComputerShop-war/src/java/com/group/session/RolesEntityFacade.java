/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.RolesEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author vancong
 */
@Stateless
public class RolesEntityFacade extends AbstractFacade<RolesEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolesEntityFacade() {
        super(RolesEntity.class);
    }
    
    public List<RolesEntity> findByName(String name){
        javax.persistence.Query q=em.createNamedQuery("RolesEntity.findByRoleName");
        q.setParameter("roleName", "%"+name+"%");
        return q.getResultList();
    }
}
