/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.AcountsEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vancong
 */
@Stateless
public class AcountsEntityFacade extends AbstractFacade<AcountsEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AcountsEntityFacade() {
        super(AcountsEntity.class);
    }
    
    public AcountsEntity findByNamAndPassword(String username,String password){
        try{
        List<AcountsEntity> list;
        Query q=em.createNamedQuery("findByNamAndPassword");
        q.setParameter("userName", username);
        q.setParameter("password", password);
        list=q.getResultList();
        if(list!=null){
            return list.get(0);
        }
        }catch(Exception e){
            e.getMessage();
        }
        return null;
    }
    
}
