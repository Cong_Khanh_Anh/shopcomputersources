/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.CustomersEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author vancong
 */
@Stateless
public class CustomersEntityFacade extends AbstractFacade<CustomersEntity> {

    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomersEntityFacade() {
        super(CustomersEntity.class);
    }

    public List<CustomersEntity> findByName(String name) {
        javax.persistence.Query q = em.createNamedQuery("CustomersEntity.findByFirstName");
        q.setParameter("firstName", "%" + name + "%");
        return q.getResultList();
    }

    public CustomersEntity findByEmailAndPassword(String email, String password) {
        try {
            List<CustomersEntity> list;
            javax.persistence.Query q = em.createNamedQuery("CustomersEntity.findByEmailAndPassword");
            q.setParameter("email", email);
            q.setParameter("password", password);
            list = q.getResultList();
            if (list != null) {
                return list.get(0);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }
}
