/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.ProvincesEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vancong
 */
@Stateless
public class ProvincesEntityFacade extends AbstractFacade<ProvincesEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProvincesEntityFacade() {
        super(ProvincesEntity.class);
    }
    
    public List<ProvincesEntity> findByName(String name){
        Query q=em.createNamedQuery("ProvincesEntity.findByProvinceName");
        q.setParameter("provinceName", "%"+name+"%");
        return q.getResultList();
    }
}
