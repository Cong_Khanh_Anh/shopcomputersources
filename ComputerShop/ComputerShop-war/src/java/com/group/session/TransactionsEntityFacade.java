/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.TransactionsEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vancong
 */
@Stateless
public class TransactionsEntityFacade extends AbstractFacade<TransactionsEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TransactionsEntityFacade() {
        super(TransactionsEntity.class);
    }
    
    public TransactionsEntity getMax(){
        String max;
        Query q=em.createNamedQuery("TransactionsEntity.getMax");
        max=(String)q.getSingleResult();
        TransactionsEntity entity=em.find(TransactionsEntity.class, max);
        if(entity!=null)
            return entity;
        return null;
    }
    
}
