/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.OrdersEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vancong
 */
@Stateless
public class OrdersEntityFacade extends AbstractFacade<OrdersEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdersEntityFacade() {
        super(OrdersEntity.class);
    }
    
    public OrdersEntity getMax(){
        int max;
        Query q=em.createNamedQuery("OrdersEntity.getMax");
        max=(Integer)q.getSingleResult();
        OrdersEntity entity=em.find(OrdersEntity.class, max);
        if(entity!=null)
            return entity;
        return null;
    }
}
