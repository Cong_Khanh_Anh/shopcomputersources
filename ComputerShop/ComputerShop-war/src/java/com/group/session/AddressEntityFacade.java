/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.AddressEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author vancong
 */
@Stateless
public class AddressEntityFacade extends AbstractFacade<AddressEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AddressEntityFacade() {
        super(AddressEntity.class);
    }
    
}
