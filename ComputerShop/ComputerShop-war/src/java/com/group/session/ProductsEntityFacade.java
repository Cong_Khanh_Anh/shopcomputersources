/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.ProductsEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vancong
 */
@Stateless
public class ProductsEntityFacade extends AbstractFacade<ProductsEntity> {

    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductsEntityFacade() {
        super(ProductsEntity.class);
    }

    public ProductsEntity findId(Integer id) {
        List<ProductsEntity> list;
        Query q = em.createNamedQuery("ProductsEntity.findByProductId");
        q.setParameter("productId", id);
        list = q.getResultList();
        if (list != null) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public List<ProductsEntity> findByName(String name) {
        Query q = em.createNamedQuery("ProductsEntity.findByName");
        q.setParameter("name", "%" + name + "%");
        return q.getResultList();
    }
    
    public List<ProductsEntity> findByViewClick(int top){
        Query q=em.createNamedQuery("ProductsEntity.findByViewClick");
        q.setParameter("viewClick", top);
        return q.getResultList();
    }
    
}
