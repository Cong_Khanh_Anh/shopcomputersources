/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.session;

import com.group.entity.PicturesEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author vancong
 */
@Stateless
public class PicturesEntityFacade extends AbstractFacade<PicturesEntity> {
    @PersistenceContext(unitName = "ComputerShop-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PicturesEntityFacade() {
        super(PicturesEntity.class);
    }
    
    public List<PicturesEntity> findByDescription(String des){
        Query q=em.createNamedQuery("PicturesEntity.findByDescription");
        q.setParameter("description", "%"+des+"%");
        return q.getResultList();
    }
}
