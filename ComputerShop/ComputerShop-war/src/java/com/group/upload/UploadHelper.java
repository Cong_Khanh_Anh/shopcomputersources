/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.upload;

import java.io.*;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

/**
 *
 * @author vancong
 */
public class UploadHelper {

    private final int limit_max_size = 10240000;
    private final String limit_type_file = "gif|jpg|png|jpeg";

    public UploadHelper() {
    }

    public String processUpload(Part fileUpload) {
        String fileSaveData = "aa.png";
        try {
            if (fileUpload.getSize() > 0) {//check size file
                String submittedFileName = getFilename(fileUpload);
                if (checkFileType(submittedFileName)) {//check type of file 
                    if (fileUpload.getSize() > this.limit_max_size) {
                        FacesContext.getCurrentInstance().addMessage("aaa", new FacesMessage(FacesMessage.SEVERITY_INFO, "File size too large!", ""));
                    } else {
                        String currentFileName = submittedFileName;
                        String extension = currentFileName.substring(currentFileName.lastIndexOf("."), currentFileName.length());
                        long nameRadom = Calendar.getInstance().getTimeInMillis();
                        String newFileName = nameRadom + extension;
                        fileSaveData = newFileName;
                        String fileSavePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("") + "\\resources\\images\\uploads";
                        try {
                            byte[] fileContent = new byte[(int) fileUpload.getSize()];
                            InputStream in = fileUpload.getInputStream();
                            in.read(fileContent);
                            File fileToCreate = new File(fileSavePath, newFileName);
                            File folder = new File(fileSavePath);
                            if (!folder.exists()) {
                                folder.mkdirs();
                            }
                            FileOutputStream fileoutStream = new FileOutputStream(fileToCreate);
                            fileoutStream.write(fileContent);
                            fileoutStream.flush();
                            fileoutStream.close();
                            fileSaveData = newFileName;
                        } catch (Exception e) {
                            e.getMessage();
                            e.printStackTrace();
                            fileSaveData = "aa.png";
                        }
                    }
                } else {
                    fileSaveData = "aa.png";
                }
            }
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
            fileSaveData = "aa.jpg";
        }
        return fileSaveData;
    }

    private String getFilename(Part part) throws Exception {
        //To change body of generated methods, choose Tools | Templates.
        //final String partHeader = part.getHeader("content-disposition");
        //System.out.println("----- validator partHeader: " + partHeader);
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                System.out.println(filename);
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }

    private boolean checkFileType(String filename) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (filename.length() > 0) {
            String[] parts = filename.split("\\.");
            if (parts.length > 0) {
                String extention = parts[parts.length - 1];
                System.out.print(extention);
                return this.limit_type_file.contains(extention);
            }
        }
        return false;
    }
}
