package com.group.controller;

import com.group.entity.OrdersEntity;
import com.group.controller.util.JsfUtil;
import com.group.controller.util.PaginationHelper;
import com.group.entity.CustomersEntity;
import com.group.entity.DistrictsEntity;
import com.group.entity.ProvincesEntity;
import com.group.session.CustomersEntityFacade;
import com.group.session.DistrictsEntityFacade;
import com.group.session.OrdersEntityFacade;
import com.group.session.ProvincesEntityFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "ordersEntityController")
@SessionScoped
public class OrdersEntityController implements Serializable {

    private OrdersEntity current;
    private DataModel items = null;
    @EJB
    private com.group.session.OrdersEntityFacade ejbFacade;
    @EJB
    private com.group.session.ProvincesEntityFacade provinceFacade;
    @EJB
    private com.group.session.DistrictsEntityFacade districtFace;
    @EJB
    private com.group.session.CustomersEntityFacade customerFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    
    private List<ProvincesEntity> listProvince;
    private List<DistrictsEntity> listDistrict;
    private List<CustomersEntity> listCustomer;
    private List<CustomersEntity> listSearch;
    private ArrayList<SelectItem> temp3;
    
    private String name;
    
    public OrdersEntityController() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public ProvincesEntityFacade getProvinceFacade() {
        return provinceFacade;
    }

    public DistrictsEntityFacade getDistrictFacade() {
        return districtFace;
    }
    
    public CustomersEntityFacade getCustomerFacade(){
        return customerFacade;
    }
    
    public String searchResult(){
        ArrayList<OrdersEntity> temp=new ArrayList<>();
        listSearch= getCustomerFacade().findByName(name);
        for (int i=0;i<listSearch.size();i++) {
            Collection<OrdersEntity> itemOrder=listSearch.get(i).getOrdersEntityCollection();
            if(itemOrder!=null){
                for(OrdersEntity item:itemOrder)
                    temp.add(item);
            }
        }
        items=new ListDataModel(temp);
        return "managerorder";
    }
    
    public ArrayList getReturnProvince() {
        ArrayList<SelectItem> temp = new ArrayList<>();
        listProvince = getProvinceFacade().findAll();
        for (ProvincesEntity item : listProvince) {
            temp.add(new SelectItem(item, item.getProvinceName()));
        }
        return temp;
    }

    public ArrayList getReturnDistrict() {
        ArrayList<SelectItem> temp2 = new ArrayList<>();
        temp2 = temp3;
        /*listDistrict = getDistrictFacade().findAll();
         for (DistrictsEntity item2 : listDistrict) {
         temp2.add(new SelectItem(item2, item2.getDistrictName()));
         }*/
        return temp2;
    }

    public ArrayList loadDistrictBasedProvince() {
        temp3 = new ArrayList<>();
        listDistrict = getDistrictFacade().findAll();
        for (DistrictsEntity item3 : listDistrict) {
            if (item3.getProvinceId().equals(current.getProvinceId())) {
                temp3.add(new SelectItem(item3, item3.getDistrictName()));
            }
        }
        return temp3;
    }
    
    public ArrayList getReturnCustomer(){
        ArrayList<SelectItem> tempcus=new ArrayList<>();
        listCustomer= getCustomerFacade().findAll();
        for (CustomersEntity itemcus : listCustomer) {
            tempcus.add(new SelectItem(itemcus, itemcus.getLastName()+" "+itemcus.getFirstName()));
        }
        return tempcus;
    }
    
    public OrdersEntity getSelected() {
        if (current == null) {
            current = new OrdersEntity();
            selectedItemIndex = -1;
        }
        return current;
    }

    private OrdersEntityFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "managerorder";
    }

    public String prepareView() {
        current = (OrdersEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new OrdersEntity();
        selectedItemIndex = -1;
        return "addNewOrder";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("OrdersEntityCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (OrdersEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "editorder";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("OrdersEntityUpdated"));
            return "managerorder";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (OrdersEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "managerorder";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("OrdersEntityDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "managerorder";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "managerorder";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = OrdersEntity.class)
    public static class OrdersEntityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            OrdersEntityController controller = (OrdersEntityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "ordersEntityController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof OrdersEntity) {
                OrdersEntity o = (OrdersEntity) object;
                return getStringKey(o.getOrderId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + OrdersEntity.class.getName());
            }
        }

    }

}
