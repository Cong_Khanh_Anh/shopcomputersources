package com.group.controller;

import com.group.entity.PicturesEntity;
import com.group.controller.util.JsfUtil;
import com.group.controller.util.PaginationHelper;
import com.group.session.PicturesEntityFacade;
import com.group.upload.UploadHelper;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.Part;

@ManagedBean(name = "picturesEntityController")
@SessionScoped
public class PicturesEntityController implements Serializable {

    private PicturesEntity current;
    private DataModel items = null;
    @EJB
    private com.group.session.PicturesEntityFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<PicturesEntity> list;
    private String descriptionname;

    private Part p;

    public PicturesEntityController() {
        //current=new PicturesEntity();
    }

    public PicturesEntity getSelected() {
        if (current == null) {
            current = new PicturesEntity();
            //System.err.println("null");
            selectedItemIndex = -1;
        }
        return current;
    }

    private PicturesEntityFacade getFacade() {
        return ejbFacade;
    }

    public Part getP() {
        return p;
    }

    public void setP(Part p) {
        this.p = p;
    }

    public List<PicturesEntity> getList() {
        return list;
    }

    public void setList(List<PicturesEntity> list) {
        this.list = list;
    }
    
    public Integer getPictureId() {
        return current.getPictureId();
    }

    public void setPictureId(Integer pictureId) {
        current.setPictureId(pictureId);
    }

    public String getDescription() {
        return current.getDescription();
    }

    public void setDescription(String description) {
        current.setDescription(description);
    }

    public String getPicture1() {
        return current.getPicture1();
    }

    public void setPicture1(String picture1) {
        current.setPicture1(picture1);
    }

    public String getPicture2() {
        return current.getPicture2();
    }

    public void setPicture2(String picture2) {
        current.setPicture2(picture2);
    }

    public String getPicture3() {
        return current.getPicture3();
    }

    public void setPicture3(String picture3) {
        current.setPicture3(picture3);
    }

    public String getPicture4() {
        return current.getPicture4();
    }

    public void setPicture4(String picture4) {
        current.setPicture4(picture4);
    }

    public String getDescriptionname() {
        return descriptionname;
    }

    public void setDescriptionname(String descriptionname) {
        this.descriptionname = descriptionname;
    }
    
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "picturedescription";
    }

    public String searchResult(){
        list=getFacade().findByDescription(descriptionname);
        items=new ListDataModel(list);
        return "picturedescription";
    }
    
    public String prepareView() {
        current = (PicturesEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new PicturesEntity();
        selectedItemIndex = -1;
        return "addpicture";
    }

    public void processUploadPC1() {
        UploadHelper uh = new UploadHelper();
        current.setPicture1(uh.processUpload(p));
    }

    public void processUploadPC2() {
        UploadHelper uh = new UploadHelper();
        current.setPicture2(uh.processUpload(p));
    }

    public void processUploadPC3() {
        UploadHelper uh = new UploadHelper();
        current.setPicture3(uh.processUpload(p));
    }

    public void processUploadPC4() {
        UploadHelper uh = new UploadHelper();
        current.setPicture4(uh.processUpload(p));
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PicturesEntityCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (PicturesEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "editpicture";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PicturesEntityUpdated"));
            return "picturedescription";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (PicturesEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "picturedescription";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PicturesEntityDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "picturedescription";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "picturedescription";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = PicturesEntity.class)
    public static class PicturesEntityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PicturesEntityController controller = (PicturesEntityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "picturesEntityController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof PicturesEntity) {
                PicturesEntity o = (PicturesEntity) object;
                return getStringKey(o.getPictureId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + PicturesEntity.class.getName());
            }
        }

    }

}
