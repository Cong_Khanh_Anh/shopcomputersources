package com.group.controller;

import com.group.entity.OrderdetailsEntity;
import com.group.controller.util.JsfUtil;
import com.group.controller.util.PaginationHelper;
import com.group.entity.OrdersEntity;
import com.group.entity.ProductsEntity;
import com.group.session.OrderdetailsEntityFacade;
import com.group.session.OrdersEntityFacade;
import com.group.session.ProductsEntityFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "orderdetailsEntityController")
@SessionScoped
public class OrderdetailsEntityController implements Serializable {

    private OrderdetailsEntity current;
    private DataModel items = null;
    @EJB
    private com.group.session.OrderdetailsEntityFacade ejbFacade;
    @EJB
    private com.group.session.OrdersEntityFacade orderFacade;
    @EJB
    private com.group.session.ProductsEntityFacade productFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    private List<OrdersEntity> listOrder;
    private List<ProductsEntity> listProduct;
    
    public OrderdetailsEntityController() {
    }

    public OrdersEntityFacade getOrderFacade(){
        return orderFacade;
    }
    
    public ProductsEntityFacade getProductFacade(){
        return productFacade;
    }
    
    public ArrayList getReturnOrder(){
        ArrayList<SelectItem> temp=new ArrayList<>();
        listOrder=getOrderFacade().findAll();
        for (OrdersEntity item : listOrder) {
            temp.add(new SelectItem(item, item.getCustomerId().getLastName()+" " +item.getCustomerId().getFirstName()));
        }
        return temp;
    }
    
    public ArrayList getReturnProduct(){
        ArrayList<SelectItem> temp2=new ArrayList<>();
        listProduct=getProductFacade().findAll();
        for (ProductsEntity item2 : listProduct) {
            temp2.add(new SelectItem(item2, item2.getName()));
        }
        return temp2;
    }
    
    public OrderdetailsEntity getSelected() {
        if (current == null) {
            current = new OrderdetailsEntity();
            selectedItemIndex = -1;
        }
        return current;
    }

    private OrderdetailsEntityFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "manager_orderdetail";
    }

    public String prepareView() {
        current = (OrderdetailsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new OrderdetailsEntity();
        selectedItemIndex = -1;
        return "addneworderdetail";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("OrderdetailsEntityCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (OrderdetailsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "editorderdetail";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("OrderdetailsEntityUpdated"));
            return "manager_orderdetail";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (OrderdetailsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "manager_orderdetail";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("OrderdetailsEntityDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "manager_orderdetail";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "manager_orderdetail";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = OrderdetailsEntity.class)
    public static class OrderdetailsEntityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            OrderdetailsEntityController controller = (OrderdetailsEntityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "orderdetailsEntityController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof OrderdetailsEntity) {
                OrderdetailsEntity o = (OrderdetailsEntity) object;
                return getStringKey(o.getOrderDetailId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + OrderdetailsEntity.class.getName());
            }
        }

    }

}
