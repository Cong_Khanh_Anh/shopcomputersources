package com.group.controller;

import com.group.entity.AcountsEntity;
import com.group.controller.util.JsfUtil;
import com.group.controller.util.PaginationHelper;
import com.group.controller.util.md5;
import com.group.entity.EmployeesEntity;
import com.group.entity.RolesEntity;
import com.group.session.AcountsEntityFacade;
import com.group.session.EmployeesEntityFacade;
import com.group.session.RolesEntityFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "acountsEntityController")
@SessionScoped
public class AcountsEntityController implements Serializable {

    private AcountsEntity current;
    private DataModel items = null;
    @EJB
    private com.group.session.AcountsEntityFacade ejbFacade;
    @EJB
    private com.group.session.RolesEntityFacade roleFacade;
    @EJB
    private com.group.session.EmployeesEntityFacade employeeFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    private List<RolesEntity> listRoles;
    private List<EmployeesEntity> listEmployees;
    private String password;
    
    public AcountsEntityController() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public RolesEntityFacade getRoleFacade(){
        return roleFacade;
    }
    
    public EmployeesEntityFacade getEmployeeFacade(){
        return employeeFacade;
    }
    
    public ArrayList getReturnRoles(){
        listRoles=getRoleFacade().findAll();
        ArrayList<SelectItem> temp=new ArrayList<>();
        for (RolesEntity item : listRoles) {
            temp.add(new SelectItem(item, item.getRoleName()));
        }
        return temp;
    }
    
    public ArrayList getReturnEmployees(){
        listEmployees=getEmployeeFacade().findAll();
        ArrayList<SelectItem> temp2=new ArrayList<>();
        for (EmployeesEntity item2 : listEmployees) {
            temp2.add(new SelectItem(item2, item2.getFirstName()));
        }
        return temp2;
    }
    
    public AcountsEntity getSelected() {
        if (current == null) {
            current = new AcountsEntity();
            selectedItemIndex = -1;
        }
        return current;
    }

    private AcountsEntityFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "manageracount";
    }

    public String prepareView() {
        current = (AcountsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new AcountsEntity();
        selectedItemIndex = -1;
        return "addacount";
    }

    public String create() {
        try {
            String passmd5=md5.encryption(password);
            getSelected().setPassword(passmd5);
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AcountsEntityCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (AcountsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "editacount";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AcountsEntityUpdated"));
            return "manageracount";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (AcountsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "manageracount";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AcountsEntityDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "manageracount";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "manageracount";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = AcountsEntity.class)
    public static class AcountsEntityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AcountsEntityController controller = (AcountsEntityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "acountsEntityController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof AcountsEntity) {
                AcountsEntity o = (AcountsEntity) object;
                return getStringKey(o.getAcountId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + AcountsEntity.class.getName());
            }
        }

    }

}
