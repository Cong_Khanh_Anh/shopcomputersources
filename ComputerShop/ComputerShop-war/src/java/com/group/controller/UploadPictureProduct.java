/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.controller;

import com.group.upload.UploadHelper;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.Part;

/**
 *
 * @author vancong
 */
@ManagedBean
@RequestScoped
public class UploadPictureProduct {

    private String photo="";
    private Part p;
    public UploadPictureProduct() {
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Part getP() {
        return p;
    }

    public void setP(Part p) {
        this.p = p;
    }
    public void processUpload(){
        UploadHelper uh=new UploadHelper();
        this.photo=uh.processUpload(p);
        //return "addBook";
    }
}
