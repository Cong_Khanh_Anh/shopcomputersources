package com.group.controller;

import com.group.entity.CustomersEntity;
import com.group.controller.util.JsfUtil;
import com.group.controller.util.MailSender;
import com.group.controller.util.PaginationHelper;
import com.group.controller.util.md5;
import com.group.entity.DistrictsEntity;
import com.group.entity.ProvincesEntity;
import com.group.session.CustomersEntityFacade;
import com.group.session.DistrictsEntityFacade;
import com.group.session.ProvincesEntityFacade;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.event.ValueChangeEvent;

@ManagedBean(name = "customersEntityController")
@SessionScoped
public class CustomersEntityController implements Serializable {

    private CustomersEntity current;
    private DataModel items = null;
    @EJB
    private com.group.session.CustomersEntityFacade ejbFacade;
    @EJB
    private com.group.session.ProvincesEntityFacade provinceFacade;
    @EJB
    private com.group.session.DistrictsEntityFacade districtFace;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    private String name;
    private List<CustomersEntity> listcustomer;
    private List<ProvincesEntity> listProvince;
    private List<DistrictsEntity> listDistrict;
    //private ProvincesEntity provinceId;
    private ArrayList<SelectItem> temp3;
    private String password;

    public CustomersEntityController() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public ProvincesEntityFacade getProvinceFacade() {
        return provinceFacade;
    }

    public DistrictsEntityFacade getDistrictFacade() {
        return districtFace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String returnSearch() {
        listcustomer = getFacade().findByName(name);
        items = new ListDataModel(listcustomer);
        return "managercustomer";
    }

    public ArrayList getReturnProvince() {
        ArrayList<SelectItem> temp = new ArrayList<>();
        listProvince = getProvinceFacade().findAll();
        for (ProvincesEntity item : listProvince) {
            temp.add(new SelectItem(item, item.getProvinceName()));
        }
        return temp;
    }

    public ArrayList getReturnDistrict() {
        ArrayList<SelectItem> temp2 = new ArrayList<>();
        temp2 = temp3;
        /*listDistrict = getDistrictFacade().findAll();
         for (DistrictsEntity item2 : listDistrict) {
         temp2.add(new SelectItem(item2, item2.getDistrictName()));
         }*/
        return temp2;
    }

    public ArrayList loadDistrictBasedProvince() {
        temp3 = new ArrayList<>();
        listDistrict = getDistrictFacade().findAll();
        for (DistrictsEntity item3 : listDistrict) {
            if (item3.getProvinceId().equals(current.getProvinceId())) {
                temp3.add(new SelectItem(item3, item3.getDistrictName()));
            }
        }
        return temp3;
    }

    public CustomersEntity getSelected() {
        if (current == null) {
            current = new CustomersEntity();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CustomersEntityFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (CustomersEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new CustomersEntity();
        selectedItemIndex = -1;
        return "addNewCustomer";
    }
    
    public String create() {
        try {
            //Date datereg=new Date(System.currentTimeMillis());
            //SimpleDateFormat fomat=new SimpleDateFormat("MM/dd/yyyy");
            //String registrydate=fomat.format(datereg);
            int status=0;
            String passmd5=md5.encryption(password);
            String email=getSelected().getEmail();
            String customerName=getSelected().getLastName()+" "+getSelected().getFirstName();
            Date date=Calendar.getInstance().getTime();
            String hashCode=md5.encryption(email+""+password);
            MailSender mailsender=new MailSender("smtp.gmail.com", "anphushopping.addministrator@gmail.com", email, "anphushopping.addministrator@gmail.com", "cong@1992");
            String contents=mailsender.getTemplateContent(email, customerName, date, hashCode);
            getSelected().setRegistryDate(date);
            getSelected().setStatus(status);
            getSelected().setPassword(passmd5);
            getSelected().setHashCode(hashCode);
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CustomersEntityCreated"));
            try{
                mailsender.setPropropery();
                mailsender.send("Hello "+customerName,contents);
            }catch(Exception e)
            {
                e.printStackTrace();
                System.out.print(e.getMessage());
            }
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (CustomersEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "editcustomer";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CustomersEntityUpdated"));
            return "managercustomer";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (CustomersEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "managercustomer";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CustomersEntityDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "managercustomer";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "managercustomer";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = CustomersEntity.class)
    public static class CustomersEntityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CustomersEntityController controller = (CustomersEntityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "customersEntityController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CustomersEntity) {
                CustomersEntity o = (CustomersEntity) object;
                return getStringKey(o.getCustomerId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CustomersEntity.class.getName());
            }
        }

    }

}
