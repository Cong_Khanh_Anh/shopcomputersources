package com.group.controller;

import com.group.entity.ProductsEntity;
import com.group.controller.util.JsfUtil;
import com.group.controller.util.PaginationHelper;
import com.group.entity.CategorysEntity;
import com.group.entity.PicturesEntity;
import com.group.session.CategorysEntityFacade;
import com.group.session.PicturesEntityFacade;
import com.group.session.ProductsEntityFacade;
import com.group.upload.UploadHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.Part;

@ManagedBean(name = "productsEntityController")
@SessionScoped
public class ProductsEntityController implements Serializable {

    private ProductsEntity current;
    private DataModel items = null;
    @EJB
    private com.group.session.ProductsEntityFacade ejbFacade;
    @EJB
    private com.group.session.CategorysEntityFacade categoryFace;
    @EJB
    private com.group.session.PicturesEntityFacade pictureFace;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    private Part p;
    private List<CategorysEntity> list;
    private List<PicturesEntity> listpic;
    private String productName;
    private List<ProductsEntity> listSearch;

    public ProductsEntityController() {
    }

    public Part getP() {
        return p;
    }

    public void setP(Part p) {
        this.p = p;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    
    public CategorysEntity getCategoryId() {
        return current.getCategoryId();
    }

    public void setCategoryId(CategorysEntity categoty) {
        current.setCategoryId(categoty);
    }

    public String getRepresentPicture() {
        return current.getRepresentPicture();
    }

    public void setRepresentPicture(String representPicture) {
        current.setRepresentPicture(representPicture);
    }

    public ArrayList getReturnListcate() {
        ArrayList temp = new ArrayList();
        list = getCategoryFac().findAll();
        list.stream().forEach((item) -> {
            temp.add(new SelectItem(item, item.getName()));
        });
        return temp;
    }

    public ArrayList<SelectItem> getReturnListPic() {
        ArrayList<SelectItem> temppic = new ArrayList<>();
        listpic = getpictureFac().findAll();
        listpic.stream().forEach((item2) -> {
            temppic.add(new SelectItem(item2, item2.getDescription()));
        });
        return temppic;
    }

    public void processUpload() {
        UploadHelper uh = new UploadHelper();
        if (current == null) {
            current = new ProductsEntity();
            //current.setRepresentPicture(uh.processUpload(p));
        }
        current.setRepresentPicture(uh.processUpload(p));
    }

    public ProductsEntity getSelected() {
        if (current == null) {
            current = new ProductsEntity();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ProductsEntityFacade getFacade() {
        return ejbFacade;
    }

    private CategorysEntityFacade getCategoryFac() {
        return categoryFace;
    }

    private PicturesEntityFacade getpictureFac() {
        return pictureFace;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (ProductsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new ProductsEntity();
        selectedItemIndex = -1;
        return "addproduct";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ProductsEntityCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (ProductsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "editproduct";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ProductsEntityUpdated"));
            return "managerproducts";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (ProductsEntity) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "managerproducts";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ProductsEntityDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "managerproducts";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "managerproducts";
    }

    /*public SelectItem[] getItemsAvailableSelectMany(){
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }*/

    public String searchResult(){
        listSearch=getFacade().findByName(productName);
        items=new ListDataModel(listSearch);
        return "managerproducts";
    }
    
    public SelectItem[] getItemsAvailableSelectOne(){
        return JsfUtil.getSelectItems(categoryFace.findAll(), true);
    }

    @FacesConverter(forClass = ProductsEntity.class)
    public static class ProductsEntityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProductsEntityController controller = (ProductsEntityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "productsEntityController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ProductsEntity) {
                ProductsEntity o = (ProductsEntity) object;
                return getStringKey(o.getProductId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + ProductsEntity.class.getName());
            }
        }

    }

    @FacesConverter("ConverterCategoryEntity")
    public class ConverterCategory implements Converter {

        @Override
        public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CategorysEntityController controller = (CategorysEntityController) fc.getApplication().getELResolver().
                    getValue(fc.getELContext(), null, "categorysEntityController");
            return controller.ejbFacade.find(value);
        }

        @Override
        public String getAsString(FacesContext fc, UIComponent uic, Object o) {
            if (o instanceof CategorysEntity) {
                return null;
            }
            return String.valueOf(((CategorysEntity) o).getDescription());
        }
    }
}
