$(document).ready(function() {
	// add h2 heading to the aside
	$("aside#asidetoc").append("<h2></h2>");

	// wrap h2 text in <a> tags
	$("article.articletoc h2").wrapInner("<a></a>");
	
	// add ids to the <a> tags
	$("article.articletoc a").each (function(index) {
		var id = "heading" + (index + 1);
		$(this).attr("id", id);
	});
		
	// clone article <a> tags and insert them into the aside
	$("article.articletoc a").clone().insertAfter($("aside#asidetoc h2"));
	
	// remove the id attributes from the <a> tags
	$("aside#asidetoc a").removeAttr("id");
	
	// add the href attributes to the <a> tags
	$("aside#asidetoc a").attr("href", function(index) {
	    var href = "#heading" + (index + 1);
		return href;
    });
	
	// change the CSS for the selected topic and move the TOC
	$("aside#asidetoc a").click (function() {
		// derive the id of the selected h2 element from the <a> tag
		id = $(this).attr("href");
		
		// change the styles for the selected heading
		$(id).css({ "color": "blue", "font-size": "150%" });
				
		var h2Offset = $(id).offset().top;
		/*var asideHeight = $("aside").height();
		var articleHeight = $("article").height();
		if ((h2Offset + asideHeight) <= articleHeight) {
			asideOffset = h2Offset;
		}
		else { 
			asideOffset = articleHeight - asideHeight;
		}
		$("aside#asidetoc").css("top", asideOffset);*/
	});
})